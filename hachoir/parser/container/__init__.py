from hachoir.parser.container.asn1 import ASN1File
from hachoir.parser.container.mkv import MkvFile
from hachoir.parser.container.ogg import OggFile, OggStream
from hachoir.parser.container.riff import RiffFile
from hachoir.parser.container.swf import SwfFile
from hachoir.parser.container.realmedia import RealMediaFile

