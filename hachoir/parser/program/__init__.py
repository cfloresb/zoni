from hachoir.parser.program.elf import ElfFile
from hachoir.parser.program.exe import ExeFile
from hachoir.parser.program.python import PythonCompiledFile
from hachoir.parser.program.java import JavaCompiledClassFile
from hachoir.parser.program.prc import PRCFile
from hachoir.parser.program.nds import NdsFile

