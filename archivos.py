from os.path import getatime, getctime, getmtime, getsize
from datetime import datetime
from hashlib import sha256

from exif_py import EXIF
from hsaudiotag import auto

from hachoir.parser import createParser
from hachoir.metadata import extractMetadata

class Archivo():
    '''Representa un archivo almacenado en el PC local.
    
    Atributos:
     - ruta: Ruta donde se encuentra el archivo
     - descripción: Descripción agregada por el usuario
     - tamaño: Tamaño del archivo (en bytes)
     - fecha_creación: Fecha de creación del archivo según el S.O.
     - fecha_modificación: Fecha de modificación del archivo según el S.O.
     - fecha_acceso: Fecha de último acceso al archivo según el S.O.
     - fecha_añadido: Fecha en que se añadió el archivo a la lista
     - hash: Hash SHA256 correspondiente al contenido del archivo'''
     
    def __init__(self, ruta, descripción=''):        
        self.ruta = ruta
        self.descripción = descripción
        
        self.tamaño = getsize(ruta) #Tamaño del archivo en bytes
        
        #Fechas del archivo, convertidas a objeto datetime:
        self.fecha_creación = datetime.fromtimestamp(getctime(ruta))
        self.fecha_modificación = datetime.fromtimestamp(getmtime(ruta))
        self.fecha_acceso = datetime.fromtimestamp(getatime(ruta))
        self.fecha_añadido = datetime.now()
        
        #Nombre y extensión:
        nombre_completo = ruta.split('/')[-1]        
        #Ya que el nombre puede tener '.', tenemos que concatenar todas las partes separadas menos la última
        self.nombre = ''
        for string in nombre_completo.split('.')[:-1]:
            self.nombre += string
        self.extension = nombre_completo.split('.')[-1]
        
        #Hash del contenido del archivo:
        archivo = open(self.ruta, 'rb')
        self.hash = sha256(archivo.read()).hexdigest()
        archivo.close()
        
    def __repr__(self):
        return self.nombre+'.'+self.extension


class ArchivoAudio(Archivo):
    '''Esta clase representa a un archivo de audio. El atributo 'información'
    es un diccionario con todas las etiquetas ID3 del archivo, extraídas a través
    de la biblioteca hsaudiotag'''
    
    def __init__(self, ruta, descripción = ''):
        super().__init__(ruta, descripción)
        
        archivo = open(self.ruta, 'rb')
        self.información = vars(auto.File(archivo))
        ''' La biblioteca hsaudiotag mantiene una referencia al archivo que
        no permite guardar la información con pickle. Eliminaremos este campo:'''
        del self.información['original']
        archivo.close()


class ArchivoFoto(Archivo):
    '''Esta clase representa a un archivo de imagen. El atributo 'información'
    es un diccionario con todas las etiquetas EXIF del archivo, extraídas a través
    de la biblioteca exif_py. Notar que el valor de cada campo del diccionario es un objeto
    EXIF_Tag'''
    def __init__(self, ruta, descripción = ''):
        super().__init__(ruta, descripción)
        
        archivo = open(self.ruta, 'rb')
        self.información = EXIF.process_file(archivo)
        archivo.close()


class ArchivoVideo(Archivo):
    '''Esta clase representa a un archivo de vídeo. El atributo 'información'
    es una lista con todos la información que es posible extraer del archivo. Los valores
    que se almacenan en esta lista son strings que incluyen su descripción.'''
    def __init__(self, ruta, descripción = ''):
        super().__init__(ruta, descripción)
        
        parser = createParser(ruta)
        self.información = extractMetadata(parser).exportPlaintext(human = False)
