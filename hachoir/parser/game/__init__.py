from hachoir.parser.game.zsnes import ZSNESFile
from hachoir.parser.game.spider_man_video import SpiderManVideoFile
from hachoir.parser.game.laf import LafFile
from hachoir.parser.game.blp import BLP1File, BLP2File