from datetime import datetime
import pickle
from archivos import ArchivoAudio, ArchivoFoto, ArchivoVideo
from os.path import isfile
import string

class ListaReproduccion():
    '''Esta clase representa una lista de reproducción, 
    incorporando todos los métodos necesarios para manipular 
    los archivos que la componen.
    
    Atributos:
     - lista: Lista (list) donde se almacenan los objetos de tipo Archivo
     - fecha_creación: Objeto datetime.datetime con la fecha de creación de la lista'''
    def __init__(self):
        self.lista = []
        self.fecha_creación = datetime.now()
    
    def agregar_archivo(self, ruta, descripción):
        '''Recibe una ruta y descripción de un archivo y crea el objeto
        archivo.Archivo correspondiente (Audio, Video, Foto).
        
        Gatilla una excepción en caso que el tipo de archivo no esté soportado
        (revisa por tipo de extensión) o en caso que el archivo ya esté ingresado
        en la lista.'''
        
        #Primero revisamos si el archivo existe:
        if not isfile(ruta):
            raise Exception('Archivo no encontrado')
        
        #Luego creamos el objeto de la clase correspondiente al archivo:
        extensiones_audio = ['wav', 'mp3', 'flac']
        extensiones_video = ['mp4', 'avi', 'mkv']
        extensiones_foto =  ['jpg', 'gif', 'png', 'bmp']
        
        extension_archivo = ruta.split('.')[-1]
        
        if extension_archivo in extensiones_audio:
            archivo = ArchivoAudio(ruta, descripción)
        elif extension_archivo in extensiones_video:
            archivo = ArchivoVideo(ruta, descripción)
        elif extension_archivo in extensiones_foto:
            archivo = ArchivoFoto(ruta, descripción)
        else:
            raise Exception('Tipo de archivo no soportado')
        
        #Si el archivo aún no está en la lista lo agregamos, en caso contrario excepción:
        if self.buscar_archivo(archivo) == False:
            self.lista.append(archivo)
        else:
            raise Exception('El archivo ya está en la lista')
        
    def buscar_archivo(self, archivo):
        '''Busca si el archivo ya se encuentra en la lista, comparando los contenidos
        registrados a través de los hash SHA256 calculados para cada archivo.
        
        Retorna la posición en la lista en caso que el archivo exista o False en caso
        contrario'''
        
        for indice, elemento in enumerate(self.lista):
            if elemento.hash == archivo.hash:
                return True
        return False
    
    def eliminar_archivo(self, archivo):
        '''Recibe un objeto de tipo archivo y lo elimina si es que está registrado en la lista'''
        
        self.lista.remove(archivo)
        
    def guardar(self, ruta):
        '''Guarda la lista como archivo pickle en la ruta indicada'''
        
        archivo = open(ruta, 'wb')
        pickle.dump(self, archivo)
        archivo.close()
        
    def cargar(self, ruta):
        '''Lee un archivo pickle donde debería estar guardada una lista de reproducción.
        Recorre la lista y agrega los archivos que allí encuentre que aún no estén cargados
        en la lista actual'''
        
        archivo = open(ruta, 'rb')
        lista_cargada = pickle.load(archivo)
        archivo.close()
        
        #Recorremos la lista cargada y, si el archivo no está en la lista actual, lo agregamos:
        for archivo in lista_cargada.lista:
            if not self.buscar_archivo(archivo):
                self.lista.append(archivo)
                
    def buscar_por_string(self, string):
        '''Recibe un string y devuelve un listado con todos los archivos que
         tengan ese string como dato en algún campo'''
        archivos_encontrados = []
        
        '''Ya que los campos información pueden ser de distinto tipo (dict o list),
        hay que implementar una lógica especial para cada caso'''
        for archivo in self.lista:

            # Recorremos los valores que tengan los atributos de cada uno de los archivos:
            for valor in vars(archivo).values():
                # Si el atributo es un string, buscamos lo pedido directamente:
                if isinstance(valor, str):
                    if string.lower() in valor.lower():
                        # Encontramos el string: agregamos el archivo a resultados y pasamos al siguiente ciclo
                        archivos_encontrados.append(archivo)
                        break
                # Si el atributo es un diccionario, pasamos todos los valores a una lista:
                elif isinstance(valor, dict):
                    valor = valor.values()

                    # Ahora recorremos la lista buscando el string:
                    for elemento in valor:
                        match = False
                        # Sólo buscamos un string si el elemento es otro string
                        if isinstance(elemento, str):
                            # Buscamos el string:
                            if string.lower() in elemento.lower():
                                # Si lo encontramos, agregamos el archivo y pasamos al siguiente ciclo
                                match = True
                                break
                    if match:
                        archivos_encontrados.append(archivo)
                        break

        # Finalmente, devolvemos el listado de archivos encontrados
        return archivos_encontrados
