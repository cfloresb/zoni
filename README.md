# Dj Master Tiger Shot 5000 #

Programa de manejo de archivos multimedia

### Descripción ###

Este programa maneja archivos multimedia, tanto de fotos (.jpg, .gif, .png), como de audio (.mp3, .flac, .wav) y video (.avi, .mp4, .mkv), creando una lista de reproducción que puede exportarse a un archivo para mayor facilidad.

### Modo de uso ###

* Clonar el repositorio
* Ejecutar el programa: python3 djmaster_cli.py

### Bilbiotecas utilizadas ###

De una manera no muy hermosa, pero funcional, este repositorio contiene código de las siguientes bibliotecas de código abierto:

* [Hachoir3](https://bitbucket.org/haypo/hachoir3)
* [HsAudiotag3k](http://hg.hardcoded.net/hsaudiotag/)
* [exif_py](https://github.com/ianare/exif-py)

### Contribución y qué esperar a futuro ###

Si quieres contribuir a mejorar este programa, feliz que clones el repositorio y luego me hagas una solicitud de pull (pull request)
Como este programa se hizo en dos tardes como pauta propuesta para el ramo de Programación Avanzada de la universidad Finis Terrae del semestre 2/2014, se hizo sólo como un ejercicio rápido y, siendo sincero, no creo que evolucione a nada más. Ahora, si te motivas y quieres agregar cosas como:

* Interfaz gráfica
* Reproducción de los archivos multimedia
* Manejo de archivos fuera del computador local (URLs)
* Alguna otra pelada de cable que se te pueda ocurrir

Eres muy bienvenido.