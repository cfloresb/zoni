from lista_reproduccion import ListaReproduccion

def imprimir_menu():
    print('\n#### Menú principal ###')
        
    menu = ['Agregar un archivo',
            'Mostrar archivos agregados',
            'Revisar información de un archivo',
            'Eliminar un archivo',
            'Guardar la lista de reproducción',
            'Cargar información de una lista de reproducción',
            'Buscar un archivo por string',
            'Salir']
    
    for indice, descripción in enumerate(menu):
        print(indice + 1, ":", descripción)

def agregar_archivo(lista_reproducción):
    ruta = input('Ingrese la ruta del archivo: ')
    descripción = input('Ingrese una descripción del archivo: ')    
    lista_reproducción.agregar_archivo(ruta, descripción)
    print('Archivo agregado exitosamente')
    
def imprimir_lista_reproducción(lista_reproducción):
    if len(lista_reproducción.lista) == 0:
        print('\nAún no hay elementos en la lista')
    else:
        for índice, elemento in enumerate(lista_reproducción.lista):
            print('[{}] : {}'.format(índice+1, elemento)) #imprimimos el índice+1 (a los no-programadores no les gusta contar desde 0)
        
def consultar_información(lista_reproducción):
    índice = int(input("Ingrese el número del archivo a consultar:"))
    archivo = lista_reproducción.lista[índice-1]
    for atributo, valor in vars(archivo).items():
        print(atributo.capitalize(),' : ', valor)
                
def eliminar_archivo(lista_reproducción):
    índice = int(input("Ingrese el número del archivo a eliminar:"))
    lista_reproducción.lista.remove(lista_reproducción.lista[índice-1])
    print("Archivo eliminado exitosamente")

def guardar_lista(lista_reproducción):
    ruta = input('Ingrese la ruta del archivo a guardar:')
    lista_reproducción.guardar(ruta)
    print('El archivo ha sido guardado exitosamente')

def cargar_lista(lista_reproducción):
    ruta = input('Ingrese la ruta del archivo con la lista de reproducción a cargar:')
    lista_reproducción.cargar(ruta)
    print('La lista de reproducción ha sido cargada exitosamente')
    
def buscar_en_archivos(lista_reproducción):
    string_a_buscar = input('Ingrese el string a buscar:')
    archivos_encontrados = lista_reproducción.buscar_por_string(string_a_buscar)
    if len(archivos_encontrados) > 0:
        print('\nSe encontraron los siguientes archivos que calzan con su búsqueda:')
        for index, archivo in enumerate(archivos_encontrados):
            print('[{}] : {}'.format(index, archivo.nombre))
    else:
        print('\nNo se encontraron archivos que calcen con su búsqueda.')

lista_reproducción = ListaReproduccion()

while True:
    imprimir_menu()    
    opción = input('Ingrese la opción: ')
    
    try:
        if opción == '1':
            agregar_archivo(lista_reproducción)
        elif opción == '2':
            imprimir_lista_reproducción(lista_reproducción)
        elif opción == '3':
            consultar_información(lista_reproducción)
        elif opción == '4':
            eliminar_archivo(lista_reproducción)
        elif opción == '5':
            guardar_lista(lista_reproducción)
        elif opción == '6':
            cargar_lista(lista_reproducción)
        elif opción == '7':
            buscar_en_archivos(lista_reproducción)
        elif opción == '8':
            break
        else:
            print("Opción no válida")
    except Exception as e:
        print('\nHa ocurrido un error :(')
        print('Detalle:', e)        

print("\n¡Hasta luego!")